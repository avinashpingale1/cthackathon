package cthackathon.api.tests;

import java.io.FileReader;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import cthackathon.api.services.CompanyServices;
import io.restassured.response.Response;

//Body type is RAW and JSON
public class Challenge8 {
	@Test
	public void createCompaniesFromDataSheetAndValidate() throws CsvValidationException, IOException {
		SoftAssert softly = new SoftAssert();
		CompanyServices company = new CompanyServices();
		company.createCompanies();
		Response allCompanies= company.getAllCompanies();
		
		String dir = System.getProperty("user.dir");
		CSVReader reader = new CSVReader(new FileReader(dir + "/src/test/resources/CompaniesData.csv"));
		String[] nextLine;
		boolean found = false;
		while ((nextLine = reader.readNext()) != null) {
			String[] details = new String[3];
			String expectedCompany = nextLine[0];
			JSONArray allComp = new JSONArray(allCompanies.asString());
			found = allComp.toString().contains(expectedCompany);
			softly.assertTrue(found,"Compant not created");
		}
		softly.assertAll();
	}
	
	@Test
	public void deletePreviouslyAddedCompanies() {

	}
}
