package cthackathon.web.tests;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import cthackathon.base.SeleniumBase;
import cthackathon.utils.Waits;
import cthackathon.web.base.Constants;
import cthackathon.web.pages.HomePage;

public class Challenge1 extends SeleniumBase{
	private static Logger LOG = Logger.getLogger(Challenge1.class);
	@Test
	public void verifyIfVideoIsAmongstOneOfTheTop() throws InterruptedException {
		launchUrl("https://automatahon20.cpsatexam.org/challenge1/");
		Constants.driver.manage().window().maximize();
		HomePage home = new HomePage();
		List<String> allUrls = home.getAllVidoeUrls();
		scrollDown();
		Thread.sleep(2000);
		String loadedVideoUrl = Constants.driver.findElement(By.xpath("//div[@class='plyr__poster']")).getCssValue("background-image");
		System.out.println("Newly loaded videos url: "+loadedVideoUrl);
		Assert.assertTrue(allUrls.contains(loadedVideoUrl));
	}
	

}
