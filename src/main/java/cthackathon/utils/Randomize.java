package cthackathon.utils;

public class Randomize {
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	/**
	 * This method generates Random Alpha Numeric String of length specified by count
	 * @param count Represents length of Alpha numeric String
	 * @return Random Alpha-numeric string
	 */
	public static String randomAlphaNumeric(int count) {
		StringBuilder sb = new StringBuilder();
		while(count--!=0) {
			int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			sb.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return sb.toString();
	}
}
