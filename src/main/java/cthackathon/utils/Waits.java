package cthackathon.utils;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import cthackathon.web.base.Constants;

public class Waits {

	Constants constants = new Constants();
	public static void waitForElementToBeLoaded(By element) {
		Constants.wait = new FluentWait(Constants.driver);
		Constants.wait
		.withTimeout(Duration.ofSeconds(5))
		.pollingEvery(Duration.ofMillis(200))
		.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	public static void waitForElementToBeClickable(By element) {
		Constants.wait = new FluentWait(Constants.driver);
		Constants.wait
		.withTimeout(Duration.ofSeconds(5))
		.pollingEvery(Duration.ofMillis(200))
		.until(ExpectedConditions.elementToBeClickable(element));
	}
	
}
