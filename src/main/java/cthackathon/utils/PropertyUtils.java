package cthackathon.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyUtils {
	private static Logger LOG = Logger.getLogger(PropertyUtils.class);

	public static String getProperty(String key) {
		FileInputStream fis=null;
		String env = System.getProperty("env","");
		String filePath = "./src/main/resources/application.properties";
		if(env.isEmpty() || env==null) {
			LOG.info("Loading local application.properties");
		}else {
			filePath = "./src/main/resources/application."+env+".properties";
			LOG.info("Loading application.properties for "+env+" environment");
		}
		try {
			fis = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			LOG.info("Unable to load application.properties");
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(fis);
		} catch (IOException e) {
			LOG.error("Unable to load application.properties");
			e.printStackTrace();
		}
		return prop.getProperty(key);
	}
}
