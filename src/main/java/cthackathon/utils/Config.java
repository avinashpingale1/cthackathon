package cthackathon.utils;

public class Config {

	public static String getBrowserName() {
		return PropertyUtils.getProperty("browser");
	}
	
	public static String getApplicationUrl() {
		return PropertyUtils.getProperty("application.url");
	}
}
