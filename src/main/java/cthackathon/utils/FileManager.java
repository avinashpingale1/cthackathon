package cthackathon.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FileManager {
	private static Logger LOG = Logger.getLogger(FileManager.class);

	/**
	 * Use this method to load excel file in two dimensional object array
	 * 
	 * @param pathToFile
	 * @throws IOException
	 */
	public static String[][] readExcelToObject(String pathToFile, String sheetName) throws IOException {
		String extension = pathToFile.split("\\.")[1];
		String source = System.getProperty("user.dir") + "/src/test/resources/";
		FileInputStream fis = new FileInputStream(source + pathToFile);
		Workbook book;
		Sheet sheet;
		if (extension.equalsIgnoreCase("xls")) {
			book = new HSSFWorkbook(fis);
			sheet = book.getSheet(sheetName);
		} else {
			book = new XSSFWorkbook(fis);
			sheet = book.getSheet(sheetName);
		}

		int lastRow = sheet.getLastRowNum();
		String[][] data = new String[lastRow][sheet.getRow(1).getLastCellNum() - 1];
		for (int i = 1; i <= lastRow; i++) {
			Row row = sheet.getRow(i);
			int lastCol = row.getLastCellNum();
			for (int j = 1; j < lastCol; j++) {
				Cell cell = row.getCell(j);
				if (cell.getCellTypeEnum() == CellType.STRING) {
					data[i - 1][j - 1] = cell.getStringCellValue();
				} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
					data[i - 1][j - 1] = String.valueOf(cell.getNumericCellValue());
				}
			}
		}
		return data;
	}

	public static void main(String[] args) throws IOException {
		String[][] str = new FileManager().readExcelToObject("sampledata.xlsx", "Sheet1");
		for (int i = 0; i < str.length; i++) {
			for (int j = 0; j < str[i].length; j++) {
				System.out.print(str[i][j] + " ");
			}
			System.out.println();
		}
	}
}
