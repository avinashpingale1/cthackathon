package cthackathon.api.services;

import java.io.File;

import org.apache.commons.lang3.CharSet;
import org.apache.log4j.Logger;

import io.restassured.http.ContentType;
import io.restassured.internal.support.FileReader;
import io.restassured.response.Response;

public class ApiUtil {
	private static Logger LOG = Logger.getLogger(ApiUtil.class);
	String baseUrl = "http://34.68.51.180:4000";
	public static String getResourceAsString(String fileName) {
		String payload = null;
		LOG.info("Reading "+fileName+" payload file");
		FileReader reader = new FileReader();
		payload = reader.readToString(new File("./src/test/resources/json/" + fileName), "UTF-8");
		return payload;
	}
}
