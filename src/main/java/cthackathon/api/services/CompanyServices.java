package cthackathon.api.services;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import io.restassured.RestAssured;
import io.restassured.config.DecoderConfig;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class CompanyServices {
	private static Logger LOG = Logger.getLogger(CompanyServices.class);
	private static String baseUrl = "http://34.68.51.180:4000";
	private String getCompanies = "/api/v1/companies";

	public Response getAllCompanies() {
		return given().when().get(baseUrl + getCompanies).then().log().all().extract().response();
	}


	public void createCompany(String comapnyName, String tel, String email) {
		String body = ApiUtil.getResourceAsString("CreateCompany.json");
		
		System.out.println("Trying to Create: "+comapnyName+" "+tel+" "+email);
		String payload = String.format(body, comapnyName, tel, email);
		System.out.println("Processing: " + payload);
		Response response = given().header("Content-Type", "application/json").contentType(ContentType.JSON).when()
				.body(payload).post(baseUrl + getCompanies).then().log().all().extract().response();
		
		System.out.println("Created company: "+comapnyName);
	}

	public void createCompanies() throws CsvValidationException, IOException {
		String dir = System.getProperty("user.dir");
		CSVReader reader = new CSVReader(new FileReader(dir + "/src/test/resources/CompaniesData.csv"));
		String[] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			String[] details = new String[3];
			for (int i=0 ;i<nextLine.length;i++) {
				details[i]=nextLine[i];
			}
			createCompany(details[0], details[1], details[2]);
		}
	}

	private void getCompaniesFromDataFile() throws CsvValidationException, IOException {
		String dir = System.getProperty("user.dir");
		CSVReader reader = new CSVReader(new FileReader(dir + "/src/test/resources/CompaniesData.csv"));
		String[] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			String[] details = new String[3];
			for (int i=0 ;i<nextLine.length;i++) {
				details[i]=nextLine[i];
			}
			createCompany(details[0], details[1], details[2]);
			
		}
	}
	
	public void deleteCompany(String name) {
		String body = ApiUtil.getResourceAsString("CreateCompany.json");
		String payload = String.format(body, "TC","info@tc.com","");
		Response response = given().body(body).when().delete(baseUrl+getCompanies).then().log().all().extract().response();
		
	}
	public static void main(String[] args) throws Exception, IOException {
		new CompanyServices().deleteCompany("TC");
	}

}
