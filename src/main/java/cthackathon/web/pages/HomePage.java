package cthackathon.web.pages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cthackathon.utils.Waits;
import cthackathon.web.base.Constants;
import io.qameta.allure.Step;

public class HomePage{
	private static Logger LOG = Logger.getLogger(HomePage.class);
	public HomePage() {
		PageFactory.initElements(Constants.driver, this);
	} 
	
	public List<String> getAllVidoeUrls() {
		Waits.waitForElementToBeLoaded(By.xpath("//section[@class='elementor-section elementor-inner-section elementor-element elementor-element-74182c0 elementor-section-boxed elementor-section-height-default elementor-section-height-default']/div/div/div"));
		List<WebElement> allVidoes = Constants.driver.findElements(By.xpath("//section[@class='elementor-section elementor-inner-section elementor-element elementor-element-74182c0 elementor-section-boxed elementor-section-height-default elementor-section-height-default']/div/div/div"));
		List<String> allUrls = new ArrayList<String>();
		Iterator<WebElement> itr = allVidoes.iterator();
		int count = 0;
		while (itr.hasNext()) {
			WebElement video = itr.next();
			WebElement innerFrame = video.findElement(By.tagName("iframe"));
			String url = null;
			if(count==1) {
				Constants.driver.switchTo().frame(innerFrame);
				url = Constants.driver.findElement(By.xpath("//div[@class='ytp-cued-thumbnail-overlay-image']")).getCssValue("background-image");
				System.out.println("Url: "+url);
				Constants.driver.switchTo().defaultContent();
			}else {
				url = Constants.driver.findElement(By.xpath("//div[@class='ytp-cued-thumbnail-overlay-image']")).getCssValue("background-image");
				System.out.println("Url: "+url);
			}
			
			allUrls.add(url);
			count++;
		}
		return allUrls;
	}
}
