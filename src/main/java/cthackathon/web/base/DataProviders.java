package cthackathon.web.base;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import cthackathon.utils.FileManager;

public class DataProviders {
	@DataProvider(name = "LoginData")
	public Object[][] loginDataProvider() throws IOException {
		return FileManager.readExcelToObject("sampledata.xlsx", "Sheet1");
	}

}
