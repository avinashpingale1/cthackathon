package cthackathon.base;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import bsh.Capabilities;
import cthackathon.base.exceptions.InvalidBrowserNameException;
import cthackathon.utils.Config;
import cthackathon.utils.PropertyUtils;
import cthackathon.web.base.Constants;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBase {
	private final static Logger LOG = Logger.getLogger(SeleniumBase.class);
	private static final String remoteUrl = "http://192.168.0.4:4444/wd/hub";
	
	@Parameters("browser")
	@BeforeMethod
	public void setup(@Optional String browserName) throws MalformedURLException {
		if(browserName.equals(null) || browserName.isEmpty()) {
			launchBrowser(Config.getBrowserName());
		}
		launchBrowser(browserName);
	}

	private void launchBrowser(String browserName) throws MalformedURLException {
		LOG.info("Opening " + browserName + " browser");
		if (browserName.trim().equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			Constants.driver = new ChromeDriver();
		} else if (browserName.trim().equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			Constants.driver = new FirefoxDriver();
		} else if (browserName.trim().equalsIgnoreCase("IE")) {
			WebDriverManager.iedriver().setup();
			Constants.driver = new InternetExplorerDriver();
		} else if (browserName.equalsIgnoreCase("RemoteFirefox")) {
			WebDriverManager.firefoxdriver().setup();
			DesiredCapabilities cap = new DesiredCapabilities().firefox();
			Constants.driver = new RemoteWebDriver(new URL(remoteUrl), cap);
		} else if (browserName.equalsIgnoreCase("RemoteChrome")) {
			WebDriverManager.chromedriver().seleniumServerStandalone().setup();
			
			DesiredCapabilities cap = new DesiredCapabilities().chrome();
			
			Constants.driver = new RemoteWebDriver(new URL(remoteUrl), cap);
		} else if (browserName.equalsIgnoreCase("RemoteIE")) {
			WebDriverManager.iedriver().clearDriverCache().setup();
			DesiredCapabilities cap = new DesiredCapabilities().internetExplorer();
			Constants.driver = new RemoteWebDriver(new URL(remoteUrl), cap);
		} else
			throw new InvalidBrowserNameException();
	}

	/**
	 * Use this method to launch the application url specified in config file
	 */
	public void launchUrl() {
		Constants.driver.get(Config.getApplicationUrl());
		LOG.info("Launching url: " + Config.getApplicationUrl());
	}

	/**
	 * Use this method to launch specified url
	 * 
	 * @param url {@code String} representation of the url
	 */
	public void launchUrl(String url) {
		Constants.driver.get(url);
		LOG.info("Launching url: " + Config.getApplicationUrl());
	}

	@AfterMethod
	public void tearDown() {
		Constants.driver.quit();;
		LOG.info("Closing browser window");
	}
	
	protected void scrollPageVertically(int pixels) {
		JavascriptExecutor jse = (JavascriptExecutor)Constants.driver;
		jse.executeScript("window.scrollBy(0,arguments[0]);", pixels);
	}
	
	public void scrollDown() {
		JavascriptExecutor jse = (JavascriptExecutor)Constants.driver;
		jse.executeScript("window.scrollBy(0,400);");
	}
	protected void scrollPageHorizontally(int pixels) {
		JavascriptExecutor jse = (JavascriptExecutor)Constants.driver;
		jse.executeScript("window.scrollBy(arguments[0],0);", pixels);
	}
	
	protected void scrollToBottom() {
		JavascriptExecutor jse = (JavascriptExecutor)Constants.driver;
		jse.executeScript("window.scrollTo(0,document.body.scrollHeight);");
	}
}
