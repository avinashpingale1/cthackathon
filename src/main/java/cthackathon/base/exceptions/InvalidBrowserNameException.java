package cthackathon.base.exceptions;
/**
 * Thrown to indicate that browser name mentioned in application.properties file in invalid.
 * Browser name must be one of the following
 * <ol>
 * <li>Chrome</li>
 * <li>Firefox</li>
 * <li>IE</li>
 * <li>Safari</li>
 * <li>All. All can be used for cross browser testing or parallel testing</li>
 * </ol>
 * @author apingale
 *
 */
public class InvalidBrowserNameException extends RuntimeException{

	@Override
	public String getMessage() {
		return "Invalid browser name mentioned in application.properties";
	}
}
